import './style.scss';

const $elementViews = document.getElementById('views');
const $elementPrice = document.getElementById('price');
const $inputRange = document.getElementById('range');
const $inputSwitch = document.getElementById('switch');

const price = [8, 12, 16, 24, 36];
const values = [2, 4, 6, 8, 10];
const views = ['10K', '50K', '100K', '500K', '1M'];

const range = (array, view) => {
  $inputRange.addEventListener('mousemove', (event) => {
    const value = parseInt(event.target.value);
    for (const item of values) {
      if (item === value) {
        let index = values.indexOf(item);
        $elementPrice.textContent = `$${array[index]}.00`;
        $elementViews.textContent = `${view[index]} Pageviews`;
      }
    }
  });
};

$inputSwitch.addEventListener('click', (event) => {
  if (event.target.checked) {
    const descuento = price.map((item) => (25 * item) / 100);
    range(descuento, views);
  } else {
    range(price, views);
  }
});
range(price, views);
